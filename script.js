const rk4 = f => (y,dt) => (
    k1 => (
        k2 => (
            k3 => (
                k4 => (k1+2.0*k2+2.0*k3+k4)/6.0
                )(dt*f(y+k3))
            )(dt*f(y+k2/2.0))
        )(dt*f(y+k1/2.0))
    )(dt*f(y))


// Para o pêndulo usaremos x -> theta e y -> omega
const fx = y => y
const fy = x => -Math.sin(x)
const dx = rk4(fx)
const dy = rk4(fy)


window.addEventListener("load", function() {
    // Pega o objeto no HTML
    var svgObject = document.getElementById('svg-object').contentDocument;
    console.log(svgObject)
    // Pega a linha
    var linha = svgObject.getElementById('linha-1');
    // Pega o círculo
    var circulo = svgObject.getElementById('circulo-1');
    // console.log(linha.getAttributeNS(null, 'y2'));
    
    // Condições iniciais
    var y = parseFloat(linha.getAttributeNS(null, 'y2'))
    var x = 250.0 - parseFloat(linha.getAttributeNS(null, 'x2'))
    var tau = 5*Math.PI/1000.0

    var theta = Math.atan(x/y)
    var wa = 0.5

    console.log(theta)

    const frame = () => {

        // let theta_a = theta

        theta = theta + dx(wa,tau)
        wa = wa + dy(theta, tau)

        y = 300*Math.cos(theta)
        x = 250 - 300*Math.sin(theta)

        linha.setAttributeNS(null,'y2',y)
        linha.setAttributeNS(null,'x2',x)
        circulo.setAttributeNS(null,'cy',y)
        circulo.setAttributeNS(null,'cx',x)
        
    }
    var id = setInterval(frame,5)
    // console.log(frame())
});


// Uma amostra do runge-kutta
{
    let x = 0
    let y = 3
    let tau = 50*Math.PI/200.0
    let i = 0
    let lista = new Array

    while (i < 200) {
        x = x + dx(y,tau)
        y = y + dy(x,tau)
        lista.push([Math.sin(x),-Math.cos(y)])
        i++
    }
// console.log(lista)
}


